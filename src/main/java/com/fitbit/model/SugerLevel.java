package com.fitbit.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by amir on 27/06/17.
 */
@Entity
@Table(name = "suger_level")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt"},
        allowGetters = true)
public class SugerLevel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String readingPeriod;

    private float suger_reading;

    @Column(nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date createdAt;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getReadingPeriod() {
        return readingPeriod;
    }

    public void setReadingPeriod(String period) {
        this.readingPeriod = period;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public float getSuger_reading() {
        return suger_reading;
    }

    public void setSuger_reading(float suger_reading) {
        this.suger_reading = suger_reading;
    }
}
