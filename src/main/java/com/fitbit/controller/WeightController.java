package com.fitbit.controller;

/**
 * Created by Amir Vahid Dastjerdi on 2/12/2017.
 */
import com.fitbit.model.Weight;
import com.fitbit.repository.SugerRepository;
import com.fitbit.repository.WeightRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;


/**
 * Created by rajeevkumarsingh on 27/06/17.
 */
@RestController
@RequestMapping("/api")
public class WeightController {

    @Autowired
    WeightRepository weightRepository;

    @GetMapping("/testweight")
    public void createWeightEntry() {
        Weight test = new Weight();
        test.setWeight_reading(85);
        test.setCreatedAt(new Date());
        Weight out = weightRepository.save(test);
    }
    @PostMapping("/writeweight")
    public Weight writeWeight(@Valid @RequestBody Weight weight) {
        weight.setCreatedAt(new Date());
        return weightRepository.save(weight);
    }

    @GetMapping("/readallweights")
    public List<Weight> getAllReadings() {
        return weightRepository.findAll();
    }




//    @DeleteMapping("/notes/{id}")
//    public ResponseEntity<Note> deleteNote(@PathVariable(value = "id") Long noteId) {
//        Note note = noteRepository.findOne(noteId);
//        if(note == null) {
////            return ResponseEntity.notFound().build();
//        }
//
//        noteRepository.delete(note);
//        return ResponseEntity.ok().build();
//        return "deleted";
//    }
}
