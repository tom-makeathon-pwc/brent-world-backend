package com.fitbit.controller;

/**
 * Created by Amir Vahid Dastjerdi on 2/12/2017.
 */
import com.fitbit.model.Step;
import com.fitbit.model.SugerLevel;
import com.fitbit.repository.StepRepository;
import com.fitbit.repository.SugerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;

/**
 * Created by rajeevkumarsingh on 27/06/17.
 */
@RestController
@RequestMapping("/api")
public class StepController {

    @Autowired
    StepRepository stepRepository;

    @GetMapping("/teststep")
    public void createStepEntry() {
        Step test = new Step();
        test.setStep_reading(124);
        test.setCreatedAt(new Date());
        Step out = stepRepository.save(test);
    }
    @PostMapping("/writestep")
    public Step createStep(@Valid @RequestBody Step step) {
        step.setCreatedAt(new Date());
        return stepRepository.save(step);
    }

    @GetMapping("/readsteps")
    public List<Step> getAllReadings() {
        return stepRepository.findAll();
    }




//    @DeleteMapping("/notes/{id}")
//    public ResponseEntity<Note> deleteNote(@PathVariable(value = "id") Long noteId) {
//        Note note = noteRepository.findOne(noteId);
//        if(note == null) {
////            return ResponseEntity.notFound().build();
//        }
//
//        noteRepository.delete(note);
//        return ResponseEntity.ok().build();
//        return "deleted";
//    }
}
