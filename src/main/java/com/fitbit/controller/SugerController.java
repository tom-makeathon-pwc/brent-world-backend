package com.fitbit.controller;

/**
 * Created by Amir Vahid Dastjerdi on 2/12/2017.
 */
import com.fitbit.model.Note;
import com.fitbit.model.SugerLevel;
import com.fitbit.repository.NoteRepository;
import com.fitbit.repository.SugerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;

/**
 * Created by rajeevkumarsingh on 27/06/17.
 */
@RestController
@RequestMapping("/api")
public class SugerController {

    @Autowired
    SugerRepository sugerRepository;

    @GetMapping("/testsuger")
    public void createSugerEntry() {
        SugerLevel test = new SugerLevel();
        test.setReadingPeriod("afternoon");
        test.setSuger_reading(11);
        test.setCreatedAt(new Date());
        SugerLevel out = sugerRepository.save(test);
    }
    @PostMapping("/sugerlevel")
    public SugerLevel createSugerLevel(@Valid @RequestBody SugerLevel sugerLevel) {
        sugerLevel.setCreatedAt(new Date());
        return sugerRepository.save(sugerLevel);
    }

    @GetMapping("/sugerreadings")
    public List<SugerLevel> getAllReadings() {
        return sugerRepository.findAll();
    }




//    @DeleteMapping("/notes/{id}")
//    public ResponseEntity<Note> deleteNote(@PathVariable(value = "id") Long noteId) {
//        Note note = noteRepository.findOne(noteId);
//        if(note == null) {
////            return ResponseEntity.notFound().build();
//        }
//
//        noteRepository.delete(note);
//        return ResponseEntity.ok().build();
//        return "deleted";
//    }
}
