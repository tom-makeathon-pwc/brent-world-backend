package com.fitbit.controller;

/**
 * Created by Amir Vahid Dastjerdi on 2/12/2017.
 */
import com.fitbit.model.SugerLevel;
import com.fitbit.repository.TrophyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.Random;

import com.fitbit.model.Trophy;
/**
 * Created by rajeevkumarsingh on 27/06/17.
 */
@RestController
@RequestMapping("/api")
public class TrophyController {

    private Random rand = new Random();

    @Autowired
    TrophyRepository trophyRepository;

    @GetMapping("/testtrophy")
    public void createTrophyEntry() {
        Trophy test = new Trophy();
        test.setDescription("Brent's First Fish");
        test.setCreatedAt(new Date());
        test.setFishIndex("0");
        Trophy out = trophyRepository.save(test);
    }
    @PostMapping("/createtrophy")
    public Trophy createTrophy(@Valid @RequestBody Trophy trophy) {
        Integer n = rand.nextInt(8);
        trophy.setFishIndex(n.toString());
        trophy.setCreatedAt(new Date());
        return trophyRepository.save(trophy);
    }

    @GetMapping("/getalltrophies")
    public List<Trophy> getAlltrophies() {
        return trophyRepository.findAll();
    }

}
