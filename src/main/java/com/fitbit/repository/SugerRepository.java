package com.fitbit.repository;

import com.fitbit.model.Step;
import com.fitbit.model.SugerLevel;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by rajeevkumarsingh on 27/06/17.
 */
public interface SugerRepository extends JpaRepository<SugerLevel, Long> {

}

