package com.fitbit.repository;

import com.fitbit.model.SugerLevel;
import com.fitbit.model.Trophy;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by amir on 27/06/17.
 */
public interface TrophyRepository extends JpaRepository<Trophy, Long> {

}

