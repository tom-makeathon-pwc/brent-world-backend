package com.fitbit.repository;

import com.fitbit.model.Step;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by amir on 27/06/17.
 */
public interface StepRepository extends JpaRepository<Step, Long> {

}

