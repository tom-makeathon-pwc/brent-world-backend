package com.fitbit.repository;

import com.fitbit.model.SugerLevel;
import com.fitbit.model.Weight;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by rajeevkumarsingh on 27/06/17.
 */
public interface WeightRepository extends JpaRepository<Weight, Long> {

}

